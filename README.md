# README #

### What is this repository for? ###

Zabrane ze swojego naturalnego środowiska rośliny potrzebują odrobiny wsparcia w postaci nawadniania oraz dostępu do swiatła. Rośliny w większych hodowlach są podłączone do systemu nawadniającego za pomocą emiterów kroplujących, których zadaniem jest równomierne nawadnianie roślin, jednak hodowca musi sam sprawdzać wilgotność gleby, temperaturę oraz inne czynniki wpływające na rośliny, po czym zdecydować czy odkręcić zawór z wodą by je nawodnić. Nasz układ ma polegać na sterowaniu podlewaniem roślin za pomocą telefonu oraz na monitorowaniu wilgotności gleby I temperatury itp za pomocą odpowiednich czujników. Jeśli wilgotność gleby oraz inne parametry są nieodpowiednie danej roślinie hodowca zostaje o tym powiadomiony. Po klinknięciu na przycisk w telefonie rośliny zostaną podlane (zawór zostaje odkręcony). Gdy rośliny zostaną dostatecznie nawodnione, użytkownik dostaje kolejne powiadomienie, po czym może zakręcić zawór.

### How do I get set up? ###

//TODO
